#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int front)	//function for input value from head.
{
	Node *data = new Node(front);
	if (head == NULL && tail = NULL)
	{
		tail = data;
		head = data;
	}
	else
	{
		data->prev = NULL;
		data->next = head;
		head = data;
	}
}

void List::tailPush(int back)	//function for input value from tail.
{
	Node *data = new Node(back);
	if (head == NULL && tail == NULL)
	{
		head = data;
		tail = head;
	}
	else
	{
		data->prev = tail;
		tail->next = data;
		tail = data;
		tail->next = NULL;
	}
}

int List::headPop()	//function for delete head value.
{
	Node *tmp = head;
	if (head == NULL && tail == NULL)
	{
		head = data;
		tail = data;
		return 0;
	}
	else
	{
		int ans = tmp->info;
		head = head->next;
		head = head->prev;
		delete tmp;
		return ans;
	}
}

int List::tailPop()	//func	tion for delete tail value.
{
	Node *tmp = tail;
	if (head == 0 && tail == 0)
	{
		head = tail = 0;
		return 0;
	}
	else
	{
		int ans = tmp->info;
		tail = tail->prev;
		tail->next = NULL;
		delete tmp;
		return ans;
	}
}

void List::deleteNode(int del)	//function for delete value that input.
{
	Node *tmp = head;
	Node *temp = head;
	Node *tem = head;
	if (del == head->info)	//input = head value.
	{
		head = head->next;
		delete tmp;
	}
	else if (tail->info == del)	//input = tail value.
	{
		while (tmp->next != 0)
		{
			temp = tmp;
			tmp = tmp->next;
		}
		tail = temp;
		temp->next = 0;
		delete tmp;
	}
	else	//input = mid value.
	{
		while (tmp->info != del)
		{
			tmp = head;
			while (tmp->info != del)
			{
				temp = tmp;
				tmp = tmp->next;
				tem = tmp->next;
			}
		}
		temp->next = tmp->next;
		tmp->prev = tem->prev;
		delete tmp;
	}
}

bool List::isInList(int val)	//function check value in the list.
{
	Node *data = new Node(*head);
	while (data != 0)
	{
		if (data->info == val)
		{
			return true;
		}
		data = data->next;
	}
	return false;
}

void List::display()	//function for display all value.
{
	Node *data = new Node(*head);
	while (data != NULL)
	{
		cout << data->info << endl;
		data = data->next;
	}
}