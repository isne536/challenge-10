#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	List test;	//value for check.
	bool value;
	int y;
	int x;
	test.headPush(1);	//call push head and show 1.
	test.tailPush(2);	//call push tail and show 2.
	test.headPush(3);	//call push head and show 3.
	test.tailPush(4);	//call push tail and show 4.
	test.display();	//check value.
	
	cout << "Input head value for delete : ";
	cout << test.headPop();	//call function delete.
	test.display();
	cout << endl;
	cout << "Input tail value for delete : ";
	cout << test.tailPop();	//call function delete.
	test.display();	//check value.
	cout << endl;
	test.headPush(5);	//call push head and show 5.
	test.headPush(6);	//call push head and show 6.
	test.tailPush(7);	//call push tail and show 7.
	test.tailPush(8);	//call push tail and show 8.
	test.display();	//check value.
	cout << "Input value to delete :";
	cin >> x;	//input delete value.
	test.deleteNode(x);	//call function for delete
	test.display();	//check value.
	cout << "Input value to check :";
	cin >> y;	//input value
	value = test.isInList(y);	//call function for check value.
	cout << endl;
	if (value == true)
	{
		cout << v << " is in the list";
	}
	else
	{
		cout << v << " isn't in the list";
	}
	cout << endl;
	system("pause");
}